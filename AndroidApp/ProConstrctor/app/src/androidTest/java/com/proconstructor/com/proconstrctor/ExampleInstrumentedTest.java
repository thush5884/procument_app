package com.proconstructor.com.proconstrctor;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.proconstructor.com.proconstrctor.Controllers.ConstructionController;
import com.proconstructor.com.proconstrctor.Controllers.ItemController;
import com.proconstructor.com.proconstrctor.Controllers.OrderController;
import com.proconstructor.com.proconstrctor.Controllers.SupplierController;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.proconstructor.com.proconstrctor", appContext.getPackageName());
    }

    @Test
    public void consructionGetAllTest() throws Exception {

        new ConstructionController(InstrumentationRegistry.getTargetContext()).GetAll(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                fail("Got an error");
            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {
                assertNotNull(obj);
            }
        });
    }

    @Test
    public void ItemControllerGetAllTest() throws Exception
    {
        new ItemController(InstrumentationRegistry.getTargetContext()).GetAllItems(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                  fail("Error occured");
            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },1);
    }

    @Test
    public void ItemControllerSearchTest() throws Exception
    {
        new ItemController(InstrumentationRegistry.getTargetContext()).Search(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                  fail("test failed");
            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },1,"");
    }

    @Test
    public void OrderControllerGetAll() throws Exception
    {
        new OrderController(InstrumentationRegistry.getTargetContext()).GetAllOrders(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                fail("test failed");
            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {

            }
        });
    }

    @Test
    public void SupplierControllerGetAll() throws Exception
    {
        new SupplierController(InstrumentationRegistry.getTargetContext()).GetAllSuppliers(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                fail("test failed");
            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {

            }
        });
    }

    @Test
    public void ItemControllerSearch() throws Exception
    {
        new ItemController(InstrumentationRegistry.getTargetContext()).Search(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                fail("test failed");
            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },1,"");
    }
}
