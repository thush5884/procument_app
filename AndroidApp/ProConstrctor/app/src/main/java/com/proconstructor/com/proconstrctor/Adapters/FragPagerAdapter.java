package com.proconstructor.com.proconstrctor.Adapters;



import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.SupplierDetailsFragment;
import com.proconstructor.com.proconstrctor.SupplierItemsFragment;

/**
 * Created by root on 11/13/17.
 */

public class FragPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Supplier Information", "Supplier Items" };
    private SupplierModel supplier;
    private Context context;

    public FragPagerAdapter(FragmentManager fm, SupplierModel supplier,Context context) {
        super(fm);
        this.supplier = supplier;
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return SupplierDetailsFragment.newInstance(supplier,context);
            case 1:
                return SupplierItemsFragment.newInstance(supplier,context);
            default:
                return null;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

}
