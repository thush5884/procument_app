package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.ItemOrderSearchViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.ItemsViewHolder;

import java.util.List;

/**
 * Created by root on 11/24/17.
 */

public class ItemOrderSearchAdapter extends RecyclerView.Adapter<ItemOrderSearchViewHolder>  {

    private List<ItemModel> items;
    private ClickEvents ce;

    public ItemOrderSearchAdapter(List<ItemModel> items, ClickEvents ce)
    {
        this.items = items;
        this.ce = ce;
    }

    @Override
    public ItemOrderSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.order_item_search, parent, false);

        return new ItemOrderSearchViewHolder(itemView,ce);
    }

    @Override
    public void onBindViewHolder(ItemOrderSearchViewHolder holder, int position) {
                final ItemModel item = items.get(position);
                holder.id.setText(String.valueOf(position));
                holder.name.setText(item.Name);
                holder.Select.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ce.OnClick(item);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
