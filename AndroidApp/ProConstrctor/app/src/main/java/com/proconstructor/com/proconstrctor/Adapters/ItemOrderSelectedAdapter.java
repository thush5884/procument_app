package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ClickTypeModel;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.ItemOrderSearchViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.ItemSelectedViewHolder;

import java.util.List;

/**
 * Created by root on 11/24/17.
 */

public class ItemOrderSelectedAdapter extends  RecyclerView.Adapter<ItemSelectedViewHolder> {

    public List<ItemModel> items;
    ClickEvents ce;

    public ItemOrderSelectedAdapter(List<ItemModel> items, ClickEvents ce)
    {
        this.items = items;
        this.ce = ce;
    }

    @Override
    public ItemSelectedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.order_item_added, parent, false);

        return new ItemSelectedViewHolder(itemView,ce);
    }

    @Override
    public void onBindViewHolder(ItemSelectedViewHolder holder, int position) {
            final ItemModel item = items.get(position);
            holder.id.setText(String.valueOf(position));
            holder.name.setText(item.Name);
            holder.qty.setText(String.valueOf(item.qty));
            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClickTypeModel model = new ClickTypeModel();
                    model.type = Enums.Add;
                    model.obj = item;
                     ce.OnClick(model);
                }
            });

            holder.dec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClickTypeModel model = new ClickTypeModel();
                    model.type = Enums.DEC;
                    model.obj = item;
                    ce.OnClick(model);
                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClickTypeModel model = new ClickTypeModel();
                    model.type = Enums.Delete;
                    model.obj = item;
                    ce.OnClick(model);
                }
            });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
