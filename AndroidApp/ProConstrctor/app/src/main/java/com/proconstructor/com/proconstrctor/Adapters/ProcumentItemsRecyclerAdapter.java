package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.ItemsViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.SupplierViewHolder;

import java.util.List;

/**
 * Created by root on 11/17/17.
 */

public class ProcumentItemsRecyclerAdapter extends RecyclerView.Adapter<ItemsViewHolder> {

    private List<ItemModel> items;
    private ClickEvents ce;

    public ProcumentItemsRecyclerAdapter(List<ItemModel> items,ClickEvents ce)
    {
        this.items = items;
        this.ce = ce;
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_layout, parent, false);

        return new ItemsViewHolder(itemView,ce);
    }

    @Override
    public void onBindViewHolder(ItemsViewHolder holder, int position) {
        final ItemModel item = items.get(position);
        holder.name.setText(item.Name);
        holder.unit.setText(item.Unit);
        holder.price.setText(item.Price.toString());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ce.OnClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
