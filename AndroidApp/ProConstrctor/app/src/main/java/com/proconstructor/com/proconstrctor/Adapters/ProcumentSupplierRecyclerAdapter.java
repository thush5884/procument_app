package com.proconstructor.com.proconstrctor.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.SupplierViewHolder;

import java.util.List;
import java.util.function.Supplier;

/**
 * Created by root on 11/13/17.
 */

public class ProcumentSupplierRecyclerAdapter extends RecyclerView.Adapter<SupplierViewHolder> {

    private List<SupplierModel> suppliers;
    private ClickEvents ce;

    public ProcumentSupplierRecyclerAdapter(List<SupplierModel> suppliers, ClickEvents clicke)
    {
        this.suppliers = suppliers;
        this.ce = clicke;
    }

    @Override
    public SupplierViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.supplier_item, parent, false);

        return new SupplierViewHolder(itemView,ce);

    }

    @Override
    public void onBindViewHolder(SupplierViewHolder holder, int position) {
        SupplierModel supplier = suppliers.get(position);
        holder.supplierNameText.setText(supplier.supplier_name);
        holder.myLetterText.setText(String.valueOf(supplier.supplier_name.charAt(0)));
        holder.createdAtText.setText(supplier.created_on);


    }



    @Override
    public int getItemCount() {
        return suppliers.size();
    }
}
