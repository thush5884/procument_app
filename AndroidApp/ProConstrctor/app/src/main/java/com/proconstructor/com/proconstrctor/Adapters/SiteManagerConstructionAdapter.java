package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ConstructionModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.ConstructionViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.SupplierViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 11/19/17.
 */

public class SiteManagerConstructionAdapter extends RecyclerView.Adapter<ConstructionViewHolder> {


    private List<ConstructionModel> constructions;
    private ClickEvents ce;

    public SiteManagerConstructionAdapter(List<ConstructionModel> constructions, ClickEvents clicke)
    {
        this.constructions = constructions;
        this.ce = clicke;
    }

    @Override
    public ConstructionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.constrcution_view, parent, false);


        return new ConstructionViewHolder(itemView,ce);
    }

    @Override
    public void onBindViewHolder(ConstructionViewHolder holder, int position) {
         final ConstructionModel construction = constructions.get(position);
         holder.consName.setText(construction.Name);
         //set pie chart

        List<PieEntry> entries = new ArrayList<>();

        entries.add(new PieEntry(49.0f, "Completed"));
        entries.add(new PieEntry(50.0f, "Pending"));



        PieDataSet set = new PieDataSet(entries, "");
        PieData data = new PieData(set);
        Description des = new Description();
        des.setText("");
        holder.chart.setDescription(des);
        holder.chart.setData(data);
        holder.chart.invalidate(); // refresh

        //
         holder.rLayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 ce.OnClick(construction);
             }
         });

    }

    @Override
    public int getItemCount() {
       return constructions.size();
    }
}
