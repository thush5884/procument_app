package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ConstructionModel;
import com.proconstructor.com.proconstrctor.Models.OrderModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.ConstructionViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.OrdersViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.SupplierViewHolder;

import java.util.List;

/**
 * Created by root on 11/24/17.
 */

public class SiteManagerOrdersAdapter extends RecyclerView.Adapter<OrdersViewHolder> {

    private List<OrderModel> orders;
    private ClickEvents ce;

    public SiteManagerOrdersAdapter(List<OrderModel> orders, ClickEvents clicke)
    {
        this.orders = orders;
        this.ce = clicke;
    }

    @Override
    public OrdersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.sitemanager_orders, parent, false);

        return new OrdersViewHolder(itemView,ce);
    }

    @Override
    public void onBindViewHolder(OrdersViewHolder holder, int position) {
           final OrderModel orderm = orders.get(position);
           holder.orderId.setText(String.valueOf(orderm.orderId));
           holder.suppllierName.setText(orderm.supplierName);
           holder.orderStatus.setText(orderm.orderStatus == 0 ? "Pending confirmation " : orderm.orderStatus == 1 ? "confirmed" : "declined");
           holder.remove.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   ce.OnClick(orderm.orderId);
               }
           });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
