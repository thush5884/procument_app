package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ClickTypeModel;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.Models.OrderModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.OrdersViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.SupervisorOrderViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.SupplierViewHolder;

import java.util.List;

/**
 * Created by root on 11/24/17.
 */

public class SupervisorOrderAdapter extends RecyclerView.Adapter<SupervisorOrderViewHolder> {

    List<OrderModel> orders;
    ClickEvents ce;

    public SupervisorOrderAdapter(List<OrderModel> orders , ClickEvents ce)
    {
        this.orders = orders;
        this.ce = ce;
    }

    @Override
    public SupervisorOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.supervisor_order, parent, false);

        return new SupervisorOrderViewHolder(itemView,ce);
    }

    @Override
    public void onBindViewHolder(final SupervisorOrderViewHolder holder, int position) {
        final OrderModel order= orders.get(position);
        holder.sname.setText(order.supplierName);
        holder.cname.setText(order.contructionName);
        List<ItemModel> items = order.items;
        String data = "";
        for(int i = 0 ; i < items.size() ; i++)
        {
            data = data +  "Id : "  + items.get(i).Id.toString() + " " + "Name : " +  items.get(i).Name + " " +  "Price : " + items.get(i).Price.toString() + " " + "Qty : " +  items.get(i).qty.toString() + " \n";
        }
        holder.data.setText(data);
        holder.acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClickTypeModel model = new ClickTypeModel();
                model.type = Enums.Add;
                model.obj = order;
                holder.ce.OnClick(model);
            }
        });

        holder.decBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClickTypeModel model = new ClickTypeModel();
                model.type = Enums.DEC;
                model.obj = order;
                holder.ce.OnClick(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
