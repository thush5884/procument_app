package com.proconstructor.com.proconstrctor.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Models.ConstructionModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;
import com.proconstructor.com.proconstrctor.ViewHolders.ConstructionViewHolder;
import com.proconstructor.com.proconstrctor.ViewHolders.SupplierViewHolder;

import java.util.List;

/**
 * Created by root on 11/23/17.
 */

public class SupplierAdapterOrder extends RecyclerView.Adapter<SupplierViewHolder> {

    private List<SupplierModel> suppliers;
    private ClickEvents clicke;

    public SupplierAdapterOrder(List<SupplierModel> suppliers, ClickEvents clicke)
    {
        this.suppliers = suppliers;
        this.clicke = clicke;
    }

    @Override
    public SupplierViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.constrcution_view, parent, false);


        return new SupplierViewHolder(itemView,clicke);
    }

    @Override
    public void onBindViewHolder(SupplierViewHolder holder, int position) {
        final SupplierModel supplier = suppliers.get(position);
        holder.consName.setText(supplier.supplier_name);
        holder.rLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicke.OnClick(supplier);
            }
        });
    }

    @Override
    public int getItemCount() {
        return suppliers.size();
    }
}
