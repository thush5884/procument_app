package com.proconstructor.com.proconstrctor.Auth;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.TokenModel;
import com.proconstructor.com.proconstrctor.Models.UserModel;
import com.proconstructor.com.proconstrctor.RestRequest.RestRequest;
import com.proconstructor.com.proconstrctor.Settings.Config;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by root on 11/5/17.
 */

public class Auth {

    RestRequest networkOp;
    Context context;

    public Auth(Context context)
    {
        networkOp = new RestRequest();
        this.context = context;
    }

    public void Authenticate(String username, String password, final CommonRequestResult cr)
    {
        String authUri = Config.GetUserAuthTokenUri();
        if(username.isEmpty())
        {
            username = "null";
        }
        if(password.isEmpty())
        {
            password = "null";
        }
        networkOp.MakePostRequest(authUri, Request.Method.POST, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    TokenModel to = new TokenModel();
                    to.TokenType = jo.getString("token_type");
                    to.AccessToken = jo.getString("access_token");
                    to.ExpiresIn = jo.getInt("expires_in");
                    to.RefreshToken = jo.getString("refresh_token");
                    new UserController(context).SaveUserTokenModel(to);
                    GetUserDetails(to,cr);
                    //cr.State(true,null);
                }
                catch (Exception e)
                {
                    OnError(e.getMessage());
                    cr.State(false,"Error when trying to parse token.");
                }
            }

            @Override
            public void GetError(String err) {
                OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Network Error Occured");
            }

            @Override
            public void RecevieObject(Object obj) {

            }


        },GetAuthParams(username,password));


    }

    private void GetUserDetails(TokenModel to,final CommonRequestResult cr) {

        String authUri = Config.GetUserDataToken();
        networkOp.MakePostRequest(authUri,Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {

                    //cr.State(true,null);
                    UserModel user = new UserModel();
                    user.id = jo.getInt("id");
                    user.email = jo.getString("email");
                    user.name = jo.getString("name");
                    user.company_id = jo.getInt("company_id");
                    user.role_id = jo.getInt("role_id");
                    user.created_at = jo.getString("created_at");
                    user.updated_at = jo.getString("updated_at");
                    new UserController(context).SaveUserModel(user);
                    cr.State(true,null);

                }
                catch (Exception e)
                {
                    OnError(e.getMessage());
                    cr.State(false,"Error when trying to parse user model.");
                    //cr.State(false,"Error when trying to parse token.");
                }
            }

            @Override
            public void GetError(String err) {
                OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Network Error Occured");

            }

            @Override
            public void RecevieObject(Object obj) {

            }

        },null);

    }

    public HashMap<String,String> GetAuthParams(String username,String password)
    {
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("grant_type","password");
        params.put("client_id", String.valueOf(2));
        params.put("client_secret",Config.secretKey);
        params.put("username",username);
        params.put("password",password);

        return  params;
    }

    //TODO maybe log
    public void OnError(String error)
    {
        //Toast.makeText(context, "Unfortuantely we have encountered an error.", Toast.LENGTH_SHORT).show();
    }




}
