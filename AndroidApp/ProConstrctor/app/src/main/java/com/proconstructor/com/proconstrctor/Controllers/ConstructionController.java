package com.proconstructor.com.proconstrctor.Controllers;

import android.content.Context;

import com.android.volley.Request;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Logger;
import com.proconstructor.com.proconstrctor.Models.ConstructionModel;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.RestRequest.RestRequest;
import com.proconstructor.com.proconstrctor.Settings.Config;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 11/19/17.
 */

public class ConstructionController {

    private RestRequest networkOp;
    private Context context;

    public ConstructionController(Context context)
    {
        networkOp = new RestRequest();
        this.context = context;
    }

    public void GetAll(final NetworkResponse nr) {

        String supplierUri = Config.GetConstructionUri() + "?company_id=" + new UserController(context).GetUserModel().company_id;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    List<ConstructionModel> contructions = new ArrayList<ConstructionModel>();
                    JSONArray ja = jo.getJSONArray("data");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jsonObject = ja.getJSONObject(i);
                        ConstructionModel constructionModel = new ConstructionModel();
                        constructionModel.Name = jsonObject.getString("name");
                        constructionModel.Id = jsonObject.getInt("id");
                        constructionModel.Company_id = jsonObject.getInt("company_id");

                        contructions.add(constructionModel);
                    }
                    //Toast.makeText(context, ja.toString(), Toast.LENGTH_SHORT).show();
                    nr.RecevieObject(contructions);

                } catch (Exception e) {
                    Logger.Log(e.getMessage());
                    nr.GetError("Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                nr.GetError(err);
            }

            @Override
            public void NetworkProblem() {
                nr.NetworkProblem();
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        }, null);
    }



}

