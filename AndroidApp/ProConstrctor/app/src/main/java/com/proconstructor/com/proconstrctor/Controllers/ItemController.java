package com.proconstructor.com.proconstrctor.Controllers;

import android.content.ClipData;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Logger;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.RestRequest.RestRequest;
import com.proconstructor.com.proconstrctor.Settings.Config;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 11/17/17.
 */

public class ItemController {

    private RestRequest networkOp;
    private Context context;

    public ItemController(Context context)
    {
        networkOp = new RestRequest();
        this.context = context;
    }

    public void AddItem(final CommonRequestResult cr, ItemModel item, Integer supplierid)
    {

        String supplierUri = Config.GetItemUrl();
        networkOp.MakePostRequest(supplierUri, Request.Method.POST, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    if(jo.getInt("status") == Enums.NetworkStatusSuccess)
                    {
                        cr.State(true,null);
                    }
                    else
                    {
                        cr.State(false,"response not identified.");
                    }

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Error when trying to parse token.");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Network Error Occured");
            }

            @Override
            public void RecevieObject(Object obj) {

            }


        },BuildNewSupplierParams(item,supplierid));
    }

    public HashMap<String,String> BuildNewSupplierParams(ItemModel item,Integer supplierId)
    {
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("price",String.valueOf(item.Price));
        params.put("name", item.Name);
        params.put("unit", item.Unit);
        params.put("company" ,String.valueOf(item.Company_Id));
        params.put("supplier_id" , String.valueOf(supplierId));

        return  params;
    }

    public void GetAllItems(final NetworkResponse nr,Integer supplierId)
    {
        String supplierUri = Config.GetItemUrl() + "?company_id=" + new UserController(context).GetUserModel().company_id + "&supplier_id=" + supplierId;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    List<ItemModel> items = new ArrayList<ItemModel>();
                    JSONArray ja = jo.getJSONArray("data");
                    for(int i=0; i < ja.length(); i++){
                        JSONObject jsonObject = ja.getJSONObject(i);
                        ItemModel item = new ItemModel();
                        item.Name = jsonObject.getString("name");
                        item.Unit = jsonObject.getString("unit");
                        item.Price = jsonObject.getInt("price");
                        item.Company_Id = jsonObject.getInt("company_id");
                        item.Id = jsonObject.getInt("id");

                        items.add(item);
                    }
                    //Toast.makeText(context, ja.toString(), Toast.LENGTH_SHORT).show();
                    nr.RecevieObject(items);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    nr.GetError("Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                nr.GetError(err);
            }

            @Override
            public void NetworkProblem() {
                nr.NetworkProblem();
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

    public void Delete(ItemModel item,final CommonRequestResult cr)
    {

        String supplierUri = Config.GetItemUrl() + "?id=" + item.Id;
        networkOp.MakePostRequest(supplierUri, Request.Method.DELETE, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {

                    cr.State(true,null);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Couldnt connect to server, Please check your internet connection.");
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

    public void Search(final NetworkResponse nr,Integer supplierId,String SearchText)
    {
        String supplierUri = Config.GetItemSearchUrl() + "?company_id=" + new UserController(context).GetUserModel().company_id + "&supplier_id=" + supplierId + "&stext=" + SearchText;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    List<ItemModel> items = new ArrayList<ItemModel>();
                    JSONArray ja = jo.getJSONArray("data");
                    for(int i=0; i < ja.length(); i++){
                        JSONObject jsonObject = ja.getJSONObject(i);
                        ItemModel item = new ItemModel();
                        item.Name = jsonObject.getString("name");
                        item.Unit = jsonObject.getString("unit");
                        item.Price = jsonObject.getInt("price");
                        item.Company_Id = jsonObject.getInt("company_id");
                        item.Id = jsonObject.getInt("id");

                        items.add(item);
                    }
                    //Toast.makeText(context, ja.toString(), Toast.LENGTH_SHORT).show();
                    nr.RecevieObject(items);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    nr.GetError("Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                nr.GetError(err);
            }

            @Override
            public void NetworkProblem() {
                nr.NetworkProblem();
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

}
