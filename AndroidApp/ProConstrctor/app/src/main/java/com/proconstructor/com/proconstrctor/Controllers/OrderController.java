package com.proconstructor.com.proconstrctor.Controllers;

import android.content.Context;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.proconstructor.com.proconstrctor.Auth.Auth;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Logger;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.Models.OrderModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.RestRequest.RestRequest;
import com.proconstructor.com.proconstrctor.Settings.Config;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 11/24/17.
 */

public class OrderController {

    private RestRequest networkOp;
    private Context context;

    public OrderController(Context context)
    {
        networkOp = new RestRequest();
        this.context = context;
    }


    public void AddItem(final CommonRequestResult cr, OrderModel order)
    {

        String supplierUri = Config.GetOrderUri();
        networkOp.MakePostRequest(supplierUri, Request.Method.POST, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    if(jo.getInt("status") == Enums.NetworkStatusSuccess)
                    {
                        cr.State(true,null);
                    }
                    else
                    {
                        cr.State(false,"response not identified.");
                    }

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Error when trying to parse token.");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Network Error Occured");
            }

            @Override
            public void RecevieObject(Object obj) {

            }


        },BuildNewSupplierParams(order));
    }

    public HashMap<String,String> BuildNewSupplierParams(OrderModel order)
    {
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("construction_id",String.valueOf(order.contrction_id));
        params.put("company_id" ,String.valueOf(new UserController(context).GetUserModel().company_id));
        params.put("supplier_id" , String.valueOf(order.supplier_id));
        Gson gson = new Gson();
        String json = gson.toJson(order);
        params.put("i_list" , json);

        return  params;
    }

    public void GetAllOrders(final NetworkResponse nr)
    {

        String supplierUri = Config.GetOrderUri() + "?company_id=" + new UserController(context).GetUserModel().company_id;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    List<OrderModel> orders = new ArrayList<OrderModel>();
                    JSONArray jaa = jo.getJSONArray("data");

                    for(int a = 0 ; a < jaa.length() ; a++ ) {
                        jo = jaa.getJSONObject(a);
                        OrderModel orderm = new OrderModel();
                        List<ItemModel> items = new ArrayList<ItemModel>();

                        String str = jo.getString("items");
                        JsonParser jp = new JsonParser();
                        JsonObject joo = (JsonObject) jp.parse(str);
                        orderm.orderId = jo.getInt("id");
                        orderm.contrction_id = joo.get("contrction_id").getAsInt();
                        orderm.contructionName = joo.get("contructionName").getAsString();
                        orderm.orderStatus = jo.getInt("order_status");
                        orderm.supplierName = joo.get("supplierName").getAsString();
                        orderm.supplier_id = joo.get("supplier_id").getAsInt();

                        JsonArray ja = joo.get("items").getAsJsonArray();
                        //JsonArray ja = (JsonArray) jp.parse(list);
                        for (int i = 0; i < ja.size(); i++) {
                            JsonObject o = ja.get(i).getAsJsonObject();
                            ItemModel item = new ItemModel();
                            item.Id = o.get("Id").getAsInt();
                            item.Company_Id = o.get("Company_Id").getAsInt();
                            item.Name = o.get("Name").getAsString();
                            item.Price = o.get("Price").getAsInt();
                            item.Unit = o.get("Unit").getAsString();
                            item.qty = o.get("qty").getAsInt();

                            items.add(item);

                        }
                        orderm.items = items;
                        orders.add(orderm);

                    }


                    nr.RecevieObject(orders);
                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    nr.GetError("Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                nr.GetError(err);
            }

            @Override
            public void NetworkProblem() {
                nr.NetworkProblem();
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

    public void Delete(Integer id,final CommonRequestResult cr)
    {

        String supplierUri = Config.GetOrderUri() + "?id=" + id;
        networkOp.MakePostRequest(supplierUri, Request.Method.DELETE, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {

                    cr.State(true,null);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Couldnt connect to server, Please check your internet connection.");
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

    public void Accept(Integer id,final CommonRequestResult cr)
    {

        String supplierUri = Config.GetOrderUri() + "/accept" + "?id=" + id;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {

                    cr.State(true,null);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Couldnt connect to server, Please check your internet connection.");
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

    public void Decline(Integer id,final CommonRequestResult cr)
    {

        String supplierUri = Config.GetOrderUri() + "/decline" + "?id=" + id;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {

                    cr.State(true,null);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Couldnt connect to server, Please check your internet connection.");
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

}
