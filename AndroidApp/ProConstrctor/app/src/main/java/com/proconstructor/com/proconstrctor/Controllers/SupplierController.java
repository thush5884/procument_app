package com.proconstructor.com.proconstrctor.Controllers;

import android.content.Context;

import com.android.volley.Request;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Logger;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.Models.TokenModel;
import com.proconstructor.com.proconstrctor.RestRequest.RestRequest;
import com.proconstructor.com.proconstrctor.Settings.Config;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 11/12/17.
 */

public class SupplierController {
    private RestRequest networkOp;
    private Context context;

     public SupplierController(Context context)
     {
         networkOp = new RestRequest();
         this.context = context;
     }

    public void AddNewSuplier(SupplierModel supplier,final CommonRequestResult cr)
    {

        String supplierUri = Config.GetSupplierUri();
        networkOp.MakePostRequest(supplierUri, Request.Method.POST, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                   if(jo.getInt("status") == Enums.NetworkStatusSuccess)
                   {
                       cr.State(true,null);
                   }
                   else
                   {
                       cr.State(false,"response not identified.");
                   }

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Error when trying to parse token.");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Network Error Occured");
            }

            @Override
            public void RecevieObject(Object obj) {

            }


        },BuildNewSupplierParams(supplier));
    }

    public HashMap<String,String> BuildNewSupplierParams(SupplierModel supplier)
    {
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("name",supplier.supplier_name);
        params.put("lon", String.valueOf(supplier.longititude));
        params.put("lat",String.valueOf(supplier.lattitude));
        params.put("company_id",String.valueOf(new UserController(context).GetUserModel().company_id));
        params.put("email",supplier.email);
        params.put("contact",supplier.contact_number);
        return  params;
    }

    public void GetAllSuppliers(final NetworkResponse nr)
    {

        String supplierUri = Config.GetSupplierUri() + "?company_id=" + new UserController(context).GetUserModel().company_id;
        networkOp.MakePostRequest(supplierUri, Request.Method.GET, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {
                    List<SupplierModel> suppliers = new ArrayList<SupplierModel>();
                    JSONArray ja = jo.getJSONArray("data");
                    for(int i=0; i < ja.length(); i++){
                        JSONObject jsonObject = ja.getJSONObject(i);
                        SupplierModel supplier = new SupplierModel();
                        supplier.id = jsonObject.getInt("id");
                        supplier.supplier_name = jsonObject.getString("name");
                        supplier.lattitude = Double.valueOf(jsonObject.getString("lat"));
                        supplier.longititude = Double.valueOf(jsonObject.getString("long"));
                        supplier.created_on = jsonObject.getString("created_at");
                        supplier.email = jsonObject.getString("email");
                        supplier.contact_number = jsonObject.getString("phone");
                        suppliers.add(supplier);
                    }
                    nr.RecevieObject(suppliers);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    nr.GetError("Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                nr.GetError(err);
            }

            @Override
            public void NetworkProblem() {
                nr.NetworkProblem();
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }

    public void Delete(SupplierModel supplier,final CommonRequestResult cr)
    {

        String supplierUri = Config.GetSupplierUri() + "?id=" + supplier.id;
        networkOp.MakePostRequest(supplierUri, Request.Method.DELETE, context, new NetworkResponse() {

            @Override
            public void GetResponse(JSONObject jo) {
                try {

                    cr.State(true,null);

                }
                catch (Exception e)
                {
                    Logger.Log(e.getMessage());
                    cr.State(false,"Failed to parse json");
                }
            }

            @Override
            public void GetError(String err) {
                //OnError(err);
                cr.State(false,err);
            }

            @Override
            public void NetworkProblem() {
                cr.State(false,"Couldnt connect to server, Please check your internet connection.");
            }

            @Override
            public void RecevieObject(Object obj) {

            }
        },null);
    }
}
