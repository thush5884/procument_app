package com.proconstructor.com.proconstrctor.Custom_Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.R;

/**
 * Created by root on 11/12/17.
 */

public class AddSupplierDialog extends DialogFragment implements OnMapReadyCallback {

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
    MapView mapView;
    GoogleMap map;
    CheckBox checkBox;
    MarkerOptions marker;

    private boolean mLocationPermissionGranted = false;
    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog,SupplierModel supplierModel);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_add_suplier_dialog, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.add_supplier_btn , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog dialogView = AddSupplierDialog.this.getDialog();
                        EditText supplierEditText = (EditText) dialogView.findViewById(R.id.supplier_name);
                        EditText supplierEmailEditText = (EditText) dialogView.findViewById(R.id.emailText);
                        EditText supplierContactNumber = (EditText) dialogView.findViewById(R.id.contactText);
                        SupplierModel supplier = new SupplierModel();
                        supplier.supplier_name = supplierEditText.getText().toString();
                        supplier.email = supplierEmailEditText.getText().toString();
                        supplier.contact_number = supplierContactNumber.getText().toString();

                        if(checkBox.isChecked())
                        {
                            if(marker != null)
                            {
                                //get location data
                                supplier.lattitude = marker.getPosition().latitude;
                                supplier.longititude = marker.getPosition().longitude;

                            }
                            else
                            {
                                Toast.makeText(getActivity().getApplicationContext(), R.string.location_not_selected, Toast.LENGTH_SHORT).show();

                            }

                        }
                        mListener.onDialogPositiveClick(AddSupplierDialog.this,supplier);
                    }
                })
                //we dont need negative button for now
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(AddSupplierDialog.this);
                    }
                });
        SetReferences(view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        return builder.create();
    }

    private void SetReferences(View view) {
        mapView = (MapView) view.findViewById(R.id.mapView);
        checkBox = (CheckBox) view.findViewById(R.id.checkBox);
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                map.clear();
                marker = new MarkerOptions()
                        .position(new LatLng(point.latitude, point.longitude))
                        .title(getString(R.string.selected_location));
                map.addMarker(marker);
                //System.out.println(point.latitude + "---" + point.longitude);
            }
        });
        //map.setMyLocationEnabled(true);
        GetLocationPermission();


    }



    private void GetLocationPermission() {
    /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(this.getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            map.setMyLocationEnabled(true);
            Log.d("permssion granted" ,"permission granted");
        } else {
            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Enums.LoactionPermission);
            Log.d("Permison" ,"Asking for permisson");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Enums.LoactionPermission: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    map.setMyLocationEnabled(true);
                    Toast.makeText(getActivity().getApplicationContext(), "8", Toast.LENGTH_SHORT).show();


                } else {
                    GetLocationPermission();
                    Toast.makeText(getActivity().getApplicationContext(), "0", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


}
