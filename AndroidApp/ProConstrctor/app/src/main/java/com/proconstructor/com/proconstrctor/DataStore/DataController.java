package com.proconstructor.com.proconstrctor.DataStore;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.proconstructor.com.proconstrctor.Models.TokenModel;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by root on 11/5/17.
 */

public class DataController {
    SharedPreferences mPrefs;

    public DataController(Context context)
    {
        this.mPrefs = context.getSharedPreferences("AppData",MODE_PRIVATE);
    }

    public void SaveObject(String key,Object o) {

        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(o);
        prefsEditor.putString(key, json);
        prefsEditor.commit();
    }

    public String GetValue(String key)
    {
        Gson gson = new Gson();
        String json = mPrefs.getString(key, "");
        return json;
    }

    public void Delete(String key)
    {
        mPrefs.edit().remove(key).commit();
    }


}
