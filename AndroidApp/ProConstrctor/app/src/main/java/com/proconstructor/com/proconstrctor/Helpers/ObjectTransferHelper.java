package com.proconstructor.com.proconstrctor.Helpers;

import android.content.Context;

import com.google.gson.Gson;
import com.proconstructor.com.proconstrctor.DataStore.DataController;

import java.util.Random;

/**
 * Created by root on 11/13/17.
 */

public class ObjectTransferHelper {
    Context context;

    public ObjectTransferHelper(Context context)
    {
        this.context = context;
    }

    public String Save(Object obj)
    {
        Random ran = new Random();
        String random = String.valueOf(ran.nextInt(1000));
        new DataController(context).SaveObject(random,obj);

        return random;
    }

    public Object getObject(Class type,String key)
    {
        Gson gson = new Gson();
        String js = new DataController(context).GetValue(key);
        Object o = new Object();
        if(!js.isEmpty())
        {
            o = gson.fromJson(js, type);
        }
        return o;
    }
}
