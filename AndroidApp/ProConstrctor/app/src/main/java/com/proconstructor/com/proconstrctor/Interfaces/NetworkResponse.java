package com.proconstructor.com.proconstrctor.Interfaces;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by root on 11/5/17.
 */

public interface NetworkResponse {
    public void GetResponse(JSONObject jo);
    public void GetError(String err);
    public void NetworkProblem();
    public void RecevieObject(Object obj);
}
