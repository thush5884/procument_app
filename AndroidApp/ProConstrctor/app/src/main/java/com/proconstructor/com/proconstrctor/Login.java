package com.proconstructor.com.proconstrctor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Auth.Auth;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Models.UserModel;
import com.proconstructor.com.proconstrctor.User.UserController;

public class Login extends AppCompatActivity {

    Button btnlLogin;
    EditText edEmail;
    EditText edPassword;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        MakeReferences();
        AddEventListners();
        CheckIfAlreadyActiveSession();
        dialog = new ProgressDialog(this); // this = YourActivity
    }

    private void CheckIfAlreadyActiveSession() {

        if(new UserController(this).IsAlreadyActiveSession())
        {
            SendToRelavantRole(new UserController(getApplicationContext()).GetUserModel());
        }
    }

    private void AddEventListners() {
        btnlLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginBtnClicked();
            }
        });
    }

    private void LoginBtnClicked() {
        ShowLoading();
        new Auth(this).Authenticate(edEmail.getText().toString(),edPassword.getText().toString(), new CommonRequestResult(){
            @Override
            public void State(boolean isSuccess,String error) {
                 if(isSuccess)
                 {
                     Toast.makeText(Login.this, "Login Success", Toast.LENGTH_SHORT).show();
                     SendToRelavantRole(new UserController(getApplicationContext()).GetUserModel());
                 }
                 else
                 {
                     Toast.makeText(Login.this, error, Toast.LENGTH_SHORT).show();
                 }
                 HideLoading();

            }
        });

    }

    private void ShowLoading() {

        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Signing in. Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void HideLoading()
    {
        dialog.cancel();
    }

    private void SendToRelavantRole(UserModel userModel) {

        Intent myIntent = null;
        if(userModel.role_id == Enums.siteManager)
        {
            myIntent = new Intent(Login.this, SiteManager.class);
        }
        else if(userModel.role_id == Enums.ProcumentDepartment)
        {
            myIntent = new Intent(Login.this, ProcumenDepartment.class);
            //myIntent = new Intent(Login.this, MapsActivity.class);
        }
        else if(userModel.role_id == Enums.superVisor)
        {
            myIntent = new Intent(Login.this, Supervisor.class);
        }

        //myIntent.putExtra("key", value); //Optional parameters
        Login.this.startActivity(myIntent);
    }

    private void MakeReferences() {
        btnlLogin = (Button) findViewById(R.id.btnLogin);
        edEmail = (EditText) findViewById(R.id.edEmail);
        edPassword = (EditText) findViewById(R.id.edPass);

    }
}
