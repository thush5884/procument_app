package com.proconstructor.com.proconstrctor.Models;

import java.util.List;

/**
 * Created by root on 11/24/17.
 */

public class OrderModel {
    public Integer orderId;
    public Integer supplier_id;
    public String supplierName;
    public Integer contrction_id;
    public String contructionName;
    public Integer orderStatus;
    public List<ItemModel> items;
}