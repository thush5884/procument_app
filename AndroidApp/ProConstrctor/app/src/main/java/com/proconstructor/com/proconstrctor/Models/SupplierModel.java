package com.proconstructor.com.proconstrctor.Models;

/**
 * Created by root on 11/12/17.
 */

public class SupplierModel {
    public int id;
    public String email;
    public String contact_number;
    public String supplier_name;
    public double lattitude;
    public double longititude;
    public String created_on;
}
