package com.proconstructor.com.proconstrctor.Models;

/**
 * Created by root on 11/5/17.
 */

public class TokenModel {

    public String TokenType;
    public int ExpiresIn;
    public String AccessToken;
    public String RefreshToken;
}
