package com.proconstructor.com.proconstrctor;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.google.gson.Gson;
import com.proconstructor.com.proconstrctor.Adapters.ProcumentSupplierRecyclerAdapter;
import com.proconstructor.com.proconstrctor.Controllers.SupplierController;
import com.proconstructor.com.proconstrctor.Custom_Dialogs.AddSupplierDialog;
import com.proconstructor.com.proconstrctor.Helpers.ObjectTransferHelper;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Interfaces.YesOrNo;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.ThirdPartyLib.SuperToast;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONObject;

import java.util.List;
import java.util.Random;

public class ProcumenDepartment extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,AddSupplierDialog.NoticeDialogListener  {

    private TextView profile_name;
    private TextView profile_email;
    private FloatingActionButton fab;
    private DialogFragment AddSupplierFragment;
    private ProgressDialog dialog;
    private RecyclerView recList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SupplierModel undoSupplier;
    private List<SupplierModel> suppliersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procument_department);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        dialog = new ProgressDialog(ProcumenDepartment.this);

        MakeReferences();
        RegisterForSwipableDismiss();
        DoCommonProceedures();
        LoadNaviData();
        AddEventListners();
        ShowLoading("Please wait..Loading Suppliers.");
        LoadSuppliers();

    }

    private void RegisterForSwipableDismiss() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;
                final ColorDrawable background = new ColorDrawable(Color.RED);
                background.setBounds((int)(itemView.getRight()+dX), itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                  int postition = viewHolder.getAdapterPosition();
                  undoSupplier = suppliersList.get(postition);
                  new com.proconstructor.com.proconstrctor.ThirdPartyLib.YesOrNo().AskYesOrNo(ProcumenDepartment.this, "Are you sure you want to delete?", new YesOrNo() {
                      @Override
                      public void ResponseFromUser(boolean res) {
                          if(res)
                          {
                              new SupplierController(getApplicationContext()).Delete(undoSupplier, new CommonRequestResult() {
                                  @Override
                                  public void State(boolean isSuccess, String error) {
                                      if(isSuccess)
                                      {
                                          Toast.makeText(ProcumenDepartment.this, "Deleted", Toast.LENGTH_SHORT).show();
                                          LoadSuppliers();
                                      }
                                      else
                                      {
                                          Toast.makeText(ProcumenDepartment.this, "Failed to delete : " + error, Toast.LENGTH_SHORT).show();
                                      }
                                  }
                              });
                          }
                          else
                          {
                              LoadSuppliers();
                          }
                      }
                  });

                  /*new SuperToast().ShowUndo(ProcumenDepartment.this, new SuperActivityToast.OnButtonClickListener() {
                      @Override
                      public void onClick(View view, Parcelable token) {
                           AddClicked(undoSupplier);
                      }
                  },"Supplier has been deleted.");*/


            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recList);
    }

    private void DoCommonProceedures() {
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
    }

    private void LoadSuppliers() {

        new SupplierController(getApplicationContext()).GetAllSuppliers(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {

                Logger.Log(err);
            }

            @Override
            public void NetworkProblem() {
                Logger.Log("NetWork Problem");
                Toast.makeText(ProcumenDepartment.this, "Couldnt connect to the server,Please try again later.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
                final List<SupplierModel> suppliers = (List<SupplierModel>) obj;
                suppliersList = suppliers;
                ProcumentSupplierRecyclerAdapter adapter = new ProcumentSupplierRecyclerAdapter(suppliers, new ClickEvents() {
                    @Override
                    public void OnClick(Object obj) {
                        int position = (int)obj;
                        SupplierModel supplier = suppliers.get(position);
                        //Toast.makeText(ProcumenDepartment.this, supplier.supplier_name, Toast.LENGTH_SHORT).show();
                        OpenSupplierActivity(supplier);
                    }
                });
                recList.setAdapter(adapter);
                HideLoading();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    private void OpenSupplierActivity(SupplierModel supplier) {
        Intent intent = new Intent(ProcumenDepartment.this, ProcumentViewSupplier.class);
        String key = new ObjectTransferHelper(getApplicationContext()).Save(supplier);
        intent.putExtra("supplier", key);
        startActivity(intent);

    }

    private void AddEventListners() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //       .setAction("Action", null).show();
                FabClicked();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadFromSwipeRefresh();
            }
        });

        recList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void LoadFromSwipeRefresh() {
        LoadSuppliers();

    }

    private void FabClicked() {
        AddSupplierFragment = new AddSupplierDialog();
        AddSupplierFragment.show(getFragmentManager(), String.valueOf(R.string.add_supplier_btn));
    }

    private void LoadNaviData() {
        profile_name.setText(new UserController(this).GetUserModel().name);
        profile_email.setText(new UserController(this).GetUserModel().email);
    }

    private void MakeReferences() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        profile_name = (TextView) headerView.findViewById(R.id.profile_name);
        profile_email = (TextView) headerView.findViewById(R.id.profile_email);

        recList = (RecyclerView) findViewById(R.id.supplierList);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.procument_department, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logout)
        {
            LogOut();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogOut()
    {
        new UserController(this).LogOut();
        Intent myIntent = new Intent(ProcumenDepartment.this, Login.class);
        //myIntent.putExtra("key", value); //Optional parameters
        ProcumenDepartment.this.startActivity(myIntent);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog,SupplierModel supplierModel) {
        // User touched the dialog's positive button
        AddClicked(supplierModel);

    }

    private void AddClicked(SupplierModel supplier) {
        ShowLoading("Please wait..Adding new supplier.");
        new SupplierController(getApplicationContext()).AddNewSuplier(supplier, new CommonRequestResult() {
            @Override
            public void State(boolean isSuccess, String error) {
                if(isSuccess)
                {
                    Toast.makeText(ProcumenDepartment.this, "Supplier Added", Toast.LENGTH_SHORT).show();
                    LoadSuppliers();
                }
                else
                {
                    Toast.makeText(ProcumenDepartment.this, error, Toast.LENGTH_SHORT).show();
                }
                HideLoading();
            }
        });
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
        CloseClicked();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void CloseClicked() {
        AddSupplierFragment.dismiss();
    }

    private void ShowLoading(String text) {

        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(text);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void HideLoading()
    {
        dialog.cancel();
    }

}
