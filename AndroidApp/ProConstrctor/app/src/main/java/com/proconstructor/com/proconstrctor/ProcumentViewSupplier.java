package com.proconstructor.com.proconstrctor;

import android.support.v4.view.ViewPager;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


import com.astuetz.PagerSlidingTabStrip;
import com.proconstructor.com.proconstrctor.Adapters.FragPagerAdapter;
import com.proconstructor.com.proconstrctor.DataStore.DataController;
import com.proconstructor.com.proconstrctor.Helpers.ObjectTransferHelper;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;


public class ProcumentViewSupplier extends AppCompatActivity {

    private SupplierModel supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procument_view_supplier);
        Bundle bundle = getIntent().getExtras();
        String key = bundle.getString("supplier");
        supplier = (SupplierModel) new ObjectTransferHelper(getApplicationContext()).getObject(SupplierModel.class,key);
        //after we got the object lets delete it
        //new DataController(getApplicationContext()).Delete(key);
        SetUpViewPager();
    }

    private void SetUpViewPager() {
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new FragPagerAdapter(getSupportFragmentManager(),supplier,getApplicationContext()));

        // Give the PagerSlidingTabStrip the ViewPager
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);
        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
