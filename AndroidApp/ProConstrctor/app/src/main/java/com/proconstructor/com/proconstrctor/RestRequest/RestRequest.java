package com.proconstructor.com.proconstrctor.RestRequest;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.TokenModel;
import com.proconstructor.com.proconstrctor.User.UserController;
import com.proconstructor.com.proconstrctor.VollyData.Volly;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 11/5/17.
 */

public class RestRequest {


    public void MakePostRequest(final String url , int method, final Context context, final NetworkResponse networkResponse, final HashMap<String,String> para )
    {
        StringRequest sr = new StringRequest
                (method, url,  new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try {
                            networkResponse.GetResponse(new JSONObject(response));
                        }
                        catch (Exception e)
                        {
                            networkResponse.GetError(e.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error URL : " , url);
                        Log.d("Error : " , error.toString());
                        if (error instanceof NetworkError)
                        {
                            networkResponse.NetworkProblem();
                        }
                        else if(error instanceof AuthFailureError)
                        {
                            networkResponse.GetError("Incorrect username or password");
                        }
                        else if(error instanceof VolleyError)
                        {
                            networkResponse.GetError("Server error occured.Please try again later");
                        }
                        else
                        {
                            networkResponse.GetError(error.toString());
                        }


                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return para;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                TokenModel to = new UserController(context).GetUserTokenModel();
                if(to.AccessToken != null)
                {
                    params.put("Authorization", "Bearer " + to.AccessToken);
                }
                return params;
            }

        };

        Volly.GetInstance(context).AddToRequestQueue(sr);
    }


}
