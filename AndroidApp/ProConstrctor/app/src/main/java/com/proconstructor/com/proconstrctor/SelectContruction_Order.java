package com.proconstructor.com.proconstrctor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Adapters.SiteManagerConstructionAdapter;
import com.proconstructor.com.proconstrctor.CommonData.TempData;
import com.proconstructor.com.proconstrctor.Controllers.ConstructionController;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.ConstructionModel;

import org.json.JSONObject;

import java.util.List;

public class SelectContruction_Order extends AppCompatActivity {

    public RecyclerView rList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_construction__order);
        SetReferences();
        DoCommonThingsOnStart();
        LoadConstructions();
    }

    private void LoadConstructions() {
        new ConstructionController(getApplicationContext()).GetAll(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                Toast.makeText(SelectContruction_Order.this, "err", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void NetworkProblem() {
                Toast.makeText(SelectContruction_Order.this, "Network Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
                List<ConstructionModel> constructionModelList = (List<ConstructionModel>) obj;
                PopulateRecycleViewWithData(constructionModelList);

            }
        });
    }

    private void SetReferences() {
        rList = findViewById(R.id.rList);
    }


    private void DoCommonThingsOnStart() {
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rList.setLayoutManager(llm);
    }

    private void PopulateRecycleViewWithData(List<ConstructionModel> constructionModelList) {
       rList.setAdapter(new SiteManagerConstructionAdapter(constructionModelList, new ClickEvents() {
           @Override
           public void OnClick(Object obj) {
               ConstructionModel construction = (ConstructionModel) obj;
               TempData.order.contrction_id = construction.Id;
               TempData.order.contructionName = construction.Name;
               Intent myIntent = new Intent(SelectContruction_Order.this, SelectSupplierOrder.class);
               //myIntent.putExtra("key", value); //Optional parameters
               SelectContruction_Order.this.startActivity(myIntent);
           }
       }));

    }
}
