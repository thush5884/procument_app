package com.proconstructor.com.proconstrctor;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Adapters.ItemOrderSearchAdapter;
import com.proconstructor.com.proconstrctor.Adapters.ItemOrderSelectedAdapter;
import com.proconstructor.com.proconstrctor.CommonData.TempData;
import com.proconstructor.com.proconstrctor.Controllers.ItemController;
import com.proconstructor.com.proconstrctor.Controllers.OrderController;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.ClickTypeModel;
import com.proconstructor.com.proconstrctor.Models.ItemModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SelectItemsOrder extends AppCompatActivity {

    RecyclerView rList;
    RecyclerView rList2;
    private List<ItemModel> selectedItems ;
    private Button makeOdder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_items_order);
        makeReferences();
        doCommonThings();
        LoadItemsData();
    }

    private void LoadItemsData() {
        new ItemController(getApplicationContext()).GetAllItems(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {

            }

            @Override
            public void NetworkProblem() {

            }

            @Override
            public void RecevieObject(Object obj) {

                List<ItemModel> items = (List<ItemModel>) obj;
                rList.setAdapter(new ItemOrderSearchAdapter(items, new ClickEvents() {
                    @Override
                    public void OnClick(Object obj) {
                        ItemModel item = (ItemModel) obj;
                        if(checkIfAlreadyExists(item))
                        {
                            Toast.makeText(SelectItemsOrder.this, "Already Added", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        selectedItems.add(item);
                        RefreshSelectedView();
                    }
                }));
            }
        }, TempData.order.supplier_id);
    }

    private boolean checkIfAlreadyExists(ItemModel model)
    {
        //Integer pos = 0;
        for(int i = 0 ; i < selectedItems.size() ; i++ )
        {
            if(selectedItems.get(i).Id == model.Id)
            {
                return true;
            }
        }

        return false;
    }

    private void RefreshSelectedView() {

        rList2.setAdapter(new ItemOrderSelectedAdapter(selectedItems, new ClickEvents() {
            @Override
            public void OnClick(Object obj) {
                ClickTypeModel type = (ClickTypeModel) obj;
                switch (type.type){
                    case Enums.Add:
                        IncreaseCount((ItemModel)type.obj);
                        RefreshSelectedView();
                        break;
                    case Enums.DEC:
                        DecreaseCount((ItemModel)type.obj);
                        RefreshSelectedView();
                        break;
                    case  Enums.Delete:
                        Delete((ItemModel)type.obj);
                        RefreshSelectedView();
                        break;

                };
            }
        }));
    }

    private void IncreaseCount(ItemModel obj) {
        Integer pos = 0;
        ItemModel iModel;
        for(int i = 0 ; i < selectedItems.size() ; i++ )
        {
            if(selectedItems.get(i).Id == obj.Id)
            {
                pos = i;
                break;
            }
        }
        iModel = selectedItems.get(pos);
        iModel.qty++;
        selectedItems.remove(obj);
        selectedItems.add(pos,iModel);
    }

    private void DecreaseCount(ItemModel obj) {
        Integer pos = 0;
        ItemModel iModel;
        for(int i = 0 ; i < selectedItems.size() ; i++ )
        {
            if(selectedItems.get(i).Id == obj.Id)
            {
                pos = i;
                break;
            }
        }
        iModel = selectedItems.get(pos);
        iModel.qty--;
        selectedItems.remove(obj);
        selectedItems.add(pos,iModel);


    }

    private void Delete(ItemModel obj) {
        selectedItems.remove(obj);
    }

    private void doCommonThings() {
        selectedItems = new ArrayList<ItemModel>();
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rList.setLayoutManager(llm);
        LinearLayoutManager llmm = new LinearLayoutManager(getApplicationContext());
        llmm.setOrientation(LinearLayoutManager.VERTICAL);
        rList2.setLayoutManager(llmm);
    }

    private void makeReferences() {
        rList = findViewById(R.id.recyclerView);
        rList2 = findViewById(R.id.rList2);
        makeOdder = findViewById(R.id.button7);
        makeOdder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeOrder();
            }
        });
    }

    private void MakeOrder() {

        TempData.order.items = selectedItems;
        TempData.order.orderStatus = 0;
        new OrderController(getApplicationContext()).AddItem(new CommonRequestResult() {
            @Override
            public void State(boolean isSuccess, String error) {
                if(isSuccess)
                {
                    Toast.makeText(SelectItemsOrder.this, "Added", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(SelectItemsOrder.this, SiteManager.class);
                    //myIntent.putExtra("key", value); //Optional parameters
                    SelectItemsOrder.this.startActivity(myIntent);
                    return;
                }
                Toast.makeText(SelectItemsOrder.this, error, Toast.LENGTH_SHORT).show();

            }
        },TempData.order);
    }
}
