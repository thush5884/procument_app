package com.proconstructor.com.proconstrctor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Adapters.SupplierAdapterOrder;
import com.proconstructor.com.proconstrctor.CommonData.TempData;
import com.proconstructor.com.proconstrctor.Controllers.SupplierController;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;

import org.json.JSONObject;

import java.util.List;

public class SelectSupplierOrder extends AppCompatActivity {

    public RecyclerView rList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_supplier_order);
        SetReferences();
        DoCommonThingsOnStart();
        LoadOnStart();

    }

    private void LoadOnStart() {
        new SupplierController(this.getApplicationContext()).GetAllSuppliers(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                Toast.makeText(SelectSupplierOrder.this, err, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void NetworkProblem() {
                Toast.makeText(SelectSupplierOrder.this, "Network error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
               List<SupplierModel> suppliers = (List<SupplierModel>) obj;
               rList.setAdapter(new SupplierAdapterOrder(suppliers, new ClickEvents() {
                   @Override
                   public void OnClick(Object obj) {
                       SupplierModel suplier = (SupplierModel) obj;
                       TempData.order.supplier_id = suplier.id;
                       TempData.order.supplierName = suplier.supplier_name;
                       Intent myIntent = new Intent(SelectSupplierOrder.this, SelectItemsOrder.class);
                       //myIntent.putExtra("key", value); //Optional parameters
                       SelectSupplierOrder.this.startActivity(myIntent);
                   }
               }));
            }
        });
    }

    private void SetReferences() {
        rList = findViewById(R.id.rList);
    }

    private void DoCommonThingsOnStart() {
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rList.setLayoutManager(llm);
    }


}
