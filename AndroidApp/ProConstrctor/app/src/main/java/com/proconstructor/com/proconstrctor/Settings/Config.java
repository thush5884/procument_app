package com.proconstructor.com.proconstrctor.Settings;

/**
 * Created by root on 11/5/17.
 */

public class Config {

    public static String protocol = "http://";
    public static String hostname = "192.3.63.218/UEE/procument_app/ProcumentProject/public";
    //api for interaction from laravel
    public static String userApi = "/api";
    public static String ouathapi = "/oauth";
    public static String token = "/token";
    public static String userData = "/user";
    public static String supplierUri = "/supplier";
    public static String itemUri = "/item";
    public static String itemSearch = "/item/search";
    public static String constructionAll = "/construction";
    public static String orderUri = "/order";

    public static String secretKey = "3vTDvXzjJzPgx3pKwjoo5rfwL5Y7spJz9dYLPgKV";

    public static int lClientId = 2;


    public static String GetUserAuthTokenUri()
    {
        return protocol + hostname + ouathapi + token;
    }

    public static String GetUserDataToken()
    {
        return protocol + hostname + userApi + userData;
    }

    public static String GetSupplierUri()
    {
        return protocol + hostname + userApi + supplierUri;
    }

    public static String GetItemUrl()
    {
        return protocol + hostname + userApi + itemUri;
    }

    public static String GetItemSearchUrl()
    {
        return protocol + hostname + userApi + itemSearch;
    }

    public static String GetConstructionUri()
    {
        return protocol + hostname + userApi + constructionAll;
    }

    public static String GetOrderUri()
    {
        return protocol + hostname + userApi + orderUri;
    }


}
