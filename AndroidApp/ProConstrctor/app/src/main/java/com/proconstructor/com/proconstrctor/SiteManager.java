package com.proconstructor.com.proconstrctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Adapters.SiteManagerOrdersAdapter;
import com.proconstructor.com.proconstrctor.Controllers.OrderController;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.OrderModel;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONObject;

import java.util.List;

public class SiteManager extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView rList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_manager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                FabClicked();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        rList = findViewById(R.id.rList);
        DoCommonThingsOnStart();
        LoadOrders();
        /*Intent intent = getIntent();
        String value = intent.getStringExtra("key"); //if it's a string you stored.*/
    }

    private void LoadOrders() {
        new OrderController(getApplicationContext()).GetAllOrders(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                Toast.makeText(SiteManager.this, err, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void NetworkProblem() {
                Toast.makeText(SiteManager.this, "Network problem", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
                List<OrderModel> orders = (List<OrderModel> ) obj;
                rList.setAdapter(new SiteManagerOrdersAdapter(orders, new ClickEvents() {
                    @Override
                    public void OnClick(Object obj) {
                            new OrderController(getApplicationContext()).Delete((Integer) obj, new CommonRequestResult() {
                                @Override
                                public void State(boolean isSuccess, String error) {
                                    if(isSuccess)
                                    {
                                        Toast.makeText(SiteManager.this, "Removed", Toast.LENGTH_SHORT).show();
                                        LoadOrders();
                                        return;
                                    }
                                    Toast.makeText(SiteManager.this, error, Toast.LENGTH_SHORT).show();
                                }
                            });
                    }
                }));
            }
        });
    }

    private void FabClicked() {
        GoToSelectSuppllierOrder();
    }

    private void GoToSelectSuppllierOrder() {
        Intent intent = new Intent(SiteManager.this, SelectContruction_Order.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.site_manager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if(id == R.id.logout)
        {

            LogOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogOut()
    {
        new UserController(this).LogOut();
        Intent myIntent = new Intent(SiteManager.this, Login.class);
        //myIntent.putExtra("key", value); //Optional parameters
        SiteManager.this.startActivity(myIntent);
    }

    private void DoCommonThingsOnStart() {
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rList.setLayoutManager(llm);
    }
}
