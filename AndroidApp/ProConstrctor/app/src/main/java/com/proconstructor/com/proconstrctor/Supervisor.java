package com.proconstructor.com.proconstrctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Adapters.SupervisorOrderAdapter;
import com.proconstructor.com.proconstrctor.Controllers.OrderController;
import com.proconstructor.com.proconstrctor.Enums.Enums;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.ClickTypeModel;
import com.proconstructor.com.proconstrctor.Models.OrderModel;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Supervisor extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView rList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        rList = findViewById(R.id.rList);
        DoCommonThingsOnStart();
        LoadOrders();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.supervisor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logout)
        {
            LogOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogOut()
    {
        new UserController(this).LogOut();
        Intent myIntent = new Intent(Supervisor.this, Login.class);
        //myIntent.putExtra("key", value); //Optional parameters
        Supervisor.this.startActivity(myIntent);
    }


    private void DoCommonThingsOnStart() {
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rList.setLayoutManager(llm);
    }

    public void LoadOrders()
    {
        new OrderController(getApplicationContext()).GetAllOrders(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                Toast.makeText(Supervisor.this, err, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void NetworkProblem() {
                Toast.makeText(Supervisor.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
                  List<OrderModel> orders = (List<OrderModel>) obj;
                  List<OrderModel> copyOrders = new ArrayList<OrderModel>();
                  for(int i = 0 ; i < orders.size()  ; i++)
                  {
                      if (orders.get(i).orderStatus == 0)
                      {
                          copyOrders.add(orders.get(i));
                      }
                  }
                  orders = copyOrders;
                  rList.setAdapter(new SupervisorOrderAdapter(orders, new ClickEvents() {
                      @Override
                      public void OnClick(Object obj) {
                          ClickTypeModel model = (ClickTypeModel) obj;
                          if(model.type == Enums.Add )
                          {
                              OrderModel order =(OrderModel) model.obj;
                              new OrderController(getApplicationContext()).Accept(order.orderId, new CommonRequestResult() {
                                  @Override
                                  public void State(boolean isSuccess, String error) {
                                      if(isSuccess)
                                      {
                                          Toast.makeText(Supervisor.this, "Success", Toast.LENGTH_SHORT).show();
                                          LoadOrders();
                                          return;
                                      }
                                      Toast.makeText(Supervisor.this, "Failed to change status", Toast.LENGTH_SHORT).show();

                                  }
                              });
                          }
                          else if(model.type == Enums.DEC )
                          {
                              OrderModel order =(OrderModel) model.obj;
                              new OrderController(getApplicationContext()).Decline(order.orderId, new CommonRequestResult() {
                                  @Override
                                  public void State(boolean isSuccess, String error) {
                                      if(isSuccess)
                                      {
                                          Toast.makeText(Supervisor.this, "Success", Toast.LENGTH_SHORT).show();
                                          LoadOrders();
                                          return;
                                      }
                                      Toast.makeText(Supervisor.this, "Failed to change status", Toast.LENGTH_SHORT).show();

                                  }
                              });
                          }
                      }
                  }));
            }
        });
    }
}
