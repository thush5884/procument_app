package com.proconstructor.com.proconstrctor;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.proconstructor.com.proconstrctor.Gps.GpsHelper;
import com.proconstructor.com.proconstrctor.Helpers.ObjectTransferHelper;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SupplierDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SupplierDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SupplierDetailsFragment extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private SupplierModel supplier;
    private TextView totItemsText;
    private TextView distanceText;
    private TextView createdOnText;
    private TextView nameText;
    private TextView emailText;
    private TextView phoneText;
    private MapView mapView;
    private GoogleMap map;
    private MarkerOptions marker;

    // TODO: Rename and change types of parameters


    private OnFragmentInteractionListener mListener;

    public SupplierDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SupplierDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SupplierDetailsFragment newInstance(SupplierModel supplier,Context context) {
        SupplierDetailsFragment fragment = new SupplierDetailsFragment();
        Bundle args = new Bundle();
        String key = new ObjectTransferHelper(context).Save(supplier);
        args.putString("supplier", key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String key = getArguments().getString("supplier");
            supplier =(SupplierModel)new ObjectTransferHelper(getActivity().getApplicationContext()).getObject(SupplierModel.class,key);
        }


    }

    private void inititateMap(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.onResume();

    }

    private void MakeReferences(View view) {
        totItemsText = (TextView) view.findViewById(R.id.totItemsText);
        distanceText = (TextView) view.findViewById(R.id.distanceText);
        createdOnText = (TextView) view.findViewById(R.id.createdDateText);
        emailText = (TextView) view.findViewById(R.id.emailText);
        phoneText = (TextView) view.findViewById(R.id.phoneText);
        mapView = (MapView) view.findViewById(R.id.mapView2);
        nameText = (TextView) view.findViewById(R.id.nameText);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_supplier_details, container, false);
        MakeReferences(view);
        inititateMap(savedInstanceState);
        LoadText();
        return view;
    }

    private void LoadText() {
        //private TextView totItemsText;
        //private TextView distanceText;
        //private TextView createdOnText;
        totItemsText.setText("nan");
        distanceText.setText("no yet done");
        createdOnText.setText(supplier.created_on);
        nameText.setText(supplier.supplier_name);
        emailText.setText(supplier.email);
        phoneText.setText(supplier.contact_number);
        new GpsHelper(getContext(),getActivity()).requestSingleUpdate(
                new GpsHelper.LocationCallback() {
                    @Override public void onNewLocationAvailable(GpsHelper.GPSCoordinates location) {
                        //got gps data , now calculate distance
                        CalculateDistance(location);
                    }
                });

    }

    private void CalculateDistance(GpsHelper.GPSCoordinates location) {
        if(supplier.longititude != 0)
        {
            distanceText.setText(String.valueOf(new GpsHelper(getContext(),getActivity()).distance(supplier.lattitude,supplier.longititude,location.latitude,location.longitude,'K')).substring(0,4) + " KM");
        }
        else
        {
            distanceText.setText("Supplier location not set.");
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("map ready" , "map is ready");
        map = googleMap;
        map.getUiSettings().setAllGesturesEnabled(true);

        if(supplier.longititude != 0)
        {
            CameraPosition supplierLoc = new CameraPosition.Builder().target(new LatLng(supplier.lattitude, supplier.longititude))
                    .zoom(12.5f)
                    .bearing(0)
                    .build();
            marker = new MarkerOptions()
                    .position(new LatLng(supplier.lattitude, supplier.longititude))
                    .title(getString(R.string.selected_location));
            GoToPoint(supplierLoc);
            AddMarker(marker);
        }
        else
        {
            Toast.makeText(getActivity().getApplicationContext(), "Location data not provided.", Toast.LENGTH_SHORT).show();

        }

        //map.getUiSettings().setMyLocationButtonEnabled(true);
        /*map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                map.clear();
                marker = new MarkerOptions()
                        .position(new LatLng(point.latitude, point.longitude))
                        .title(getString(R.string.selected_location));
                map.addMarker(marker);
                //System.out.println(point.latitude + "---" + point.longitude);
            }
        });*/
    }

    private void AddMarker(MarkerOptions marker) {
        map.addMarker(marker);
    }

    public void GoToPoint(CameraPosition pos) {
        changeCamera(CameraUpdateFactory.newCameraPosition(pos), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                //Toast.makeText(getActivity().getApplicationContext(), "asdasd", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {

            }
        });
    }


    /**
     * Change the camera position by moving or animating the camera depending on the state of the
     * animate toggle button.
     */
    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback) {
                map.animateCamera(update, callback);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
