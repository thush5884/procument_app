package com.proconstructor.com.proconstrctor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.lang.UCharacter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.proconstructor.com.proconstrctor.Adapters.ProcumentItemsRecyclerAdapter;
import com.proconstructor.com.proconstrctor.Controllers.ItemController;
import com.proconstructor.com.proconstrctor.Helpers.ObjectTransferHelper;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.Interfaces.CommonRequestResult;
import com.proconstructor.com.proconstrctor.Interfaces.NetworkResponse;
import com.proconstructor.com.proconstrctor.Models.ItemModel;
import com.proconstructor.com.proconstrctor.Models.SupplierModel;
import com.proconstructor.com.proconstrctor.User.UserController;

import org.json.JSONObject;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SupplierItemsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SupplierItemsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SupplierItemsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private SupplierModel supplier;
    private OnFragmentInteractionListener mListener;
    private FloatingActionButton fab;
    private RecyclerView rList;
    private SwipeRefreshLayout refreshLayout;
    private ProgressDialog mDialog;
    private EditText SearchEditText;

    public SupplierItemsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SupplierItemsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SupplierItemsFragment newInstance(SupplierModel supplier,Context context) {
        SupplierItemsFragment fragment = new SupplierItemsFragment();
        Bundle args = new Bundle();
        String key = new ObjectTransferHelper(context).Save(supplier);
        args.putString("supplier", key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String key = getArguments().getString("supplier");
            supplier =(SupplierModel)new ObjectTransferHelper(getActivity().getApplicationContext()).getObject(SupplierModel.class,key);
        }
        else
        {
            Toast.makeText(getActivity().getApplicationContext(), "Sorry,We encountered a problem.", Toast.LENGTH_SHORT).show();
        }

    }

    private void ShowLoadingMessage(String message)
    {
        mDialog = new ProgressDialog(getContext());
        mDialog.setMessage(message);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    private void HideLoading()
    {
        mDialog.cancel();
    }


    private void LoadItemList() {

        ShowLoadingMessage("Loading items.. Please wait.");
        new ItemController(getContext()).GetAllItems(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                HideLoading();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getActivity().getApplicationContext(), "Sorry,Error occured when getting item list", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void NetworkProblem() {
                HideLoading();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getActivity().getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
                   HideLoading();
                   List<ItemModel> items = (List<ItemModel>) obj;
                   ProcumentItemsRecyclerAdapter radapter = new ProcumentItemsRecyclerAdapter(items, new ClickEvents() {
                       @Override
                       public void OnClick(Object obj) {
                           ItemModel item = (ItemModel) obj;
                           Delete(item);
                       }
                   });
                   rList.setAdapter(radapter);
                   refreshLayout.setRefreshing(false);
            }
        },supplier.id);

    }

    private void Delete(final ItemModel item) {

        new AlertDialog.Builder(getContext())
        .setTitle("Delete?")
        .setMessage("Are you sure you want to delete?")
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {

                new ItemController(getActivity().getApplicationContext()).Delete(item, new CommonRequestResult() {
                    @Override
                    public void State(boolean isSuccess, String error) {
                        if(isSuccess)
                        {
                            Toast.makeText(getActivity().getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
                            LoadItemList();
                        }
                        else
                        {
                            Toast.makeText(getActivity().getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        }

                    }
                });


            }})
        .setNegativeButton(android.R.string.no, null).show();


    }

    private void DoCommonProceedures() {
        rList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rList.setLayoutManager(llm);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_supplier_items, container, false);
        MakeReferences(view);
        SetEventListners();
        DoCommonProceedures();
        LoadItemList();
        return view;
    }

    private void SetEventListners() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FabClicked();
            }
        });
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadItemList();

            }
        });
        SearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                 Search(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void FabClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Please input item name");

        LinearLayout lila1= new LinearLayout(getContext());
        lila1.setOrientation(LinearLayout.VERTICAL); //1 is for vertical orientation
        // Set up the input
        final EditText input = new EditText(getContext());
        final EditText Unit = new EditText(getContext());
        final EditText price = new EditText(getContext());
        price.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        TextView ItemNameText = new TextView(getContext());
        TextView UnitText = new TextView(getContext());
        TextView PriceText = new TextView(getContext());

        ItemNameText.setText("Enter item name");
        UnitText.setText("Enter unit,eg: KG");
        PriceText.setText("Price");

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        lila1.addView(ItemNameText);
        lila1.addView(input);
        lila1.addView(UnitText);
        lila1.addView(Unit);
        lila1.addView(PriceText);
        lila1.addView(price);
        builder.setView(lila1);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               String text = input.getText().toString();
               String unit = Unit.getText().toString();
               Integer p = Integer.parseInt(price.getText().toString());
               ItemModel i = new ItemModel();
               i.Name = text;
               i.Price = p;
               i.Unit = unit;
               i.Company_Id = new UserController(getContext()).GetUserModel().company_id;
               if(!text.isEmpty())
               {
                   AddItem(i);
               }
               else
               {
                   Toast.makeText(getContext(), "No item provided", Toast.LENGTH_SHORT).show();
               }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void Search(String text)
    {
        new ItemController(getContext()).Search(new NetworkResponse() {
            @Override
            public void GetResponse(JSONObject jo) {

            }

            @Override
            public void GetError(String err) {
                Toast.makeText(getContext(), "Failed to search", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void NetworkProblem() {
                Toast.makeText(getContext(), "Check your internet connection", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void RecevieObject(Object obj) {
                List<ItemModel> items = (List<ItemModel>) obj;
                ProcumentItemsRecyclerAdapter radapter = new ProcumentItemsRecyclerAdapter(items, new ClickEvents() {
                    @Override
                    public void OnClick(Object obj) {
                        ItemModel item = (ItemModel) obj;
                        Delete(item);
                    }
                });
                rList.setAdapter(radapter);
                refreshLayout.setRefreshing(false);
            }
        },supplier.id,text);
    }

    private void AddItem(ItemModel item) {
        new ItemController(getContext()).AddItem(new CommonRequestResult(){

            @Override
            public void State(boolean isSuccess, String error) {
                if(isSuccess)
                {
                    Toast.makeText(getContext(), "Added Item", Toast.LENGTH_SHORT).show();
                    LoadItemList();
                }
                else
                {
                    Toast.makeText(getContext(), "Failed to add new item.", Toast.LENGTH_SHORT).show();
                }
            }
        },item,supplier.id);
    }

    private void MakeReferences(View view) {
        fab = view.findViewById(R.id.fab);
        rList = view.findViewById(R.id.rListt);
        refreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        SearchEditText = view.findViewById(R.id.searchText);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
