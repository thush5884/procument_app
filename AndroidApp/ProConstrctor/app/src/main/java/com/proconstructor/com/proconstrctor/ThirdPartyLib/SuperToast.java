package com.proconstructor.com.proconstrctor.ThirdPartyLib;

import android.app.Activity;
import android.graphics.Color;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;
import com.proconstructor.com.proconstrctor.R;

import java.util.ResourceBundle;

/**
 * Created by root on 11/13/17.
 */

public class SuperToast {

    public void ShowUndo(Activity activity,SuperActivityToast.OnButtonClickListener buttonClickListener,String message)
    {
        SuperActivityToast.create(activity, new Style(), Style.TYPE_BUTTON)
                .setButtonText("UNDO")
                //.setButtonIconResource(R.drawable.un)
                .setOnButtonClickListener("good_tag_name", null, buttonClickListener)
                .setProgressBarColor(Color.WHITE)
                .setText(message)
                .setDuration(Style.DURATION_LONG)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(PaletteUtils.getSolidColor(PaletteUtils.BLACK))
                .setAnimations(Style.ANIMATIONS_POP).show();
    }
}
