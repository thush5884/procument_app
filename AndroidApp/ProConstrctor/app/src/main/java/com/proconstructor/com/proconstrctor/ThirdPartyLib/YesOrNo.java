package com.proconstructor.com.proconstrctor.ThirdPartyLib;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by root on 11/13/17.
 */

public class YesOrNo {

    public void AskYesOrNo(Context context, String message, final com.proconstructor.com.proconstrctor.Interfaces.YesOrNo answer)
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        answer.ResponseFromUser(true);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        answer.ResponseFromUser(false);
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show()
                .setCanceledOnTouchOutside(false);
    }
}
