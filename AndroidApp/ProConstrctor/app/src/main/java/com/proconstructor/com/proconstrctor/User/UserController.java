package com.proconstructor.com.proconstrctor.User;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.proconstructor.com.proconstrctor.DataStore.DataController;
import com.proconstructor.com.proconstrctor.Models.TokenModel;
import com.proconstructor.com.proconstrctor.Models.UserModel;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by root on 11/5/17.
 */

public class UserController {

    SharedPreferences mPrefs;
    Context context;

    public UserController(Context context)
    {
        this.mPrefs = context.getSharedPreferences("AppData",MODE_PRIVATE);
        this.context = context;
    }

    public void SaveUserTokenModel(TokenModel to) {

        new DataController(context).SaveObject("UserData",to);
    }

    public TokenModel GetUserTokenModel()
    {
        Gson gson = new Gson();
        String js = new DataController(context).GetValue("UserData");
        TokenModel to = new TokenModel();
        if(!js.isEmpty())
        {
            to = gson.fromJson(js, TokenModel.class);
        }
        return to;
    }

    public void SaveUserModel(UserModel uo) {

        new DataController(context).SaveObject("UserModel",uo);
    }

    public UserModel GetUserModel()
    {
        Gson gson = new Gson();
        String js = new DataController(context).GetValue("UserModel");
        UserModel uo = new UserModel();
        if(!js.isEmpty())
        {
            uo = gson.fromJson(js, UserModel.class);
        }
        return uo;
    }

    public boolean LogOut()
    {
        new DataController(context).Delete("UserModel");
        new DataController(context).Delete("UserData");

        return true;
    }

    public boolean IsAlreadyActiveSession() {

        if(new UserController(context).GetUserModel().email != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }



}
