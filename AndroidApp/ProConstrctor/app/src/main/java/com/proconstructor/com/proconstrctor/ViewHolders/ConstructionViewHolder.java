package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.R;

import org.w3c.dom.Text;

/**
 * Created by root on 11/19/17.
 */

public class ConstructionViewHolder extends RecyclerView.ViewHolder {

    public TextView consName;
    public PieChart chart;
    public TextView percentage;
    public TextView rOrders;
    public TextView pOrders;
    public RelativeLayout rLayout;

    public ConstructionViewHolder(View itemView, final ClickEvents ce) {
        super(itemView);
        consName = (TextView) itemView.findViewById(R.id.consName);
        chart = (PieChart) itemView.findViewById(R.id.chart);
        percentage = (TextView) itemView.findViewById(R.id.percentage);
        rOrders = (TextView) itemView.findViewById(R.id.rOrders);
        pOrders = (TextView) itemView.findViewById(R.id.pOrders);
        rLayout = (RelativeLayout) itemView.findViewById(R.id.container);
    }
}
