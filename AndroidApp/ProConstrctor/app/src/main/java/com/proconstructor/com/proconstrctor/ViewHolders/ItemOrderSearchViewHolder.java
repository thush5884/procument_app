package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.R;

/**
 * Created by root on 11/24/17.
 */

public class ItemOrderSearchViewHolder  extends RecyclerView.ViewHolder {

    public TextView id;
    public TextView name;
    public Button Select;
    ClickEvents ce;


    public ItemOrderSearchViewHolder(View itemView, final ClickEvents ce) {
        super(itemView);
        this.ce = ce;
        Select = (Button) itemView.findViewById(R.id.button6);
        id = (TextView) itemView.findViewById(R.id.textView10);
        name = (TextView) itemView.findViewById(R.id.textView11);


    }
}


