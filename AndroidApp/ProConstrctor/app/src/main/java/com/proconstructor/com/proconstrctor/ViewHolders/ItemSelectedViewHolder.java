package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.R;

/**
 * Created by root on 11/24/17.
 */

public class ItemSelectedViewHolder extends RecyclerView.ViewHolder {

    public TextView id;
    public TextView name;
    public TextView qty;
    public Button add;
    public Button dec;
    public Button delete;

    public ItemSelectedViewHolder(View itemView, final ClickEvents ce) {
        super(itemView);
        id = (TextView) itemView.findViewById(R.id.textView10);
        name = (TextView) itemView.findViewById(R.id.textView11);
        qty = (TextView) itemView.findViewById(R.id.textView15);
        add = (Button)itemView.findViewById(R.id.button3);
        dec = (Button)itemView.findViewById(R.id.button4);
        delete = (Button)itemView.findViewById(R.id.button5);
    }
}
