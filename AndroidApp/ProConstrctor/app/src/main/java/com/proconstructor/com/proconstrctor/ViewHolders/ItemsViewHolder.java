package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.proconstructor.com.proconstrctor.R;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;

/**
 * Created by root on 11/17/17.
 */

public class ItemsViewHolder extends RecyclerView.ViewHolder {

    public TextView name;
    public TextView price;
    public Button delete;
    public TextView unit;

    public ItemsViewHolder(View itemView, final ClickEvents ce) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.name);
        price = (TextView) itemView.findViewById(R.id.price);
        delete = (Button) itemView.findViewById(R.id.delete);
        unit = (TextView) itemView.findViewById(R.id.unit);
    }
}
