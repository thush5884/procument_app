package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.proconstructor.com.proconstrctor.CommonData.TempData;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.R;

/**
 * Created by root on 11/24/17.
 */

public class OrdersViewHolder extends RecyclerView.ViewHolder {

    public TextView orderId;
    public TextView suppllierName;
    public TextView orderStatus;
    public Button  remove;
    public ClickEvents ce;


    public OrdersViewHolder(View itemView, ClickEvents ce) {
        super(itemView);
        orderId = (TextView) itemView.findViewById(R.id.oid);
        suppllierName = (TextView)itemView.findViewById(R.id.sid);
        orderStatus = (TextView)itemView.findViewById(R.id.status);
        remove = (Button) itemView.findViewById(R.id.removeBtn);
        this.ce = ce;
    }
}
