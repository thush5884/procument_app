package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.R;

/**
 * Created by root on 11/24/17.
 */

public class SupervisorOrderViewHolder extends RecyclerView.ViewHolder {

    public TextView sname;
    public TextView cname;
    public TextView data;
    public Button acceptBtn;
    public Button decBtn;
    public ClickEvents ce;

    public SupervisorOrderViewHolder(View itemView, ClickEvents ce) {
        super(itemView);
        sname = (TextView)itemView.findViewById(R.id.sname);
        cname = (TextView)itemView.findViewById(R.id.cname);
        data = (TextView)itemView.findViewById(R.id.data);
        acceptBtn = (Button)itemView.findViewById(R.id.acceptBtn);
        decBtn = (Button)itemView.findViewById(R.id.decBtn);
        this.ce = ce;
    }
}
