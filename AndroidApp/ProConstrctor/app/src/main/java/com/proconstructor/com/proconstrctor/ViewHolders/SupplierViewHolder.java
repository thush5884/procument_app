package com.proconstructor.com.proconstrctor.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.proconstructor.com.proconstrctor.Interfaces.ClickEvents;
import com.proconstructor.com.proconstrctor.R;

/**
 * Created by root on 11/13/17.
 */

public class SupplierViewHolder extends RecyclerView.ViewHolder{

    public TextView supplierNameText;
    public TextView myLetterText;
    public TextView createdAtText;
    public RelativeLayout rLayout;
    public TextView consName;
    public PieChart chart;
    public TextView percentage;
    public TextView rOrders;
    public TextView pOrders;


    public SupplierViewHolder(View v, final ClickEvents ce) {
        super(v);
        supplierNameText =  (TextView) v.findViewById(R.id.supplier_name);
        myLetterText = (TextView) v.findViewById(R.id.my_letter);
        createdAtText = (TextView) v.findViewById(R.id.created_at);
        consName = (TextView) itemView.findViewById(R.id.consName);
        chart = (PieChart) itemView.findViewById(R.id.chart);
        percentage = (TextView) itemView.findViewById(R.id.percentage);
        rOrders = (TextView) itemView.findViewById(R.id.rOrders);
        pOrders = (TextView) itemView.findViewById(R.id.pOrders);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ce.OnClick(getAdapterPosition());
            }
        });
        rLayout = (RelativeLayout) itemView.findViewById(R.id.container);

    }

}
