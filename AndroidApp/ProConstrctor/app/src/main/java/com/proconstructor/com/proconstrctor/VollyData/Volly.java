package com.proconstructor.com.proconstrctor.VollyData;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by root on 11/5/17.
 */

public class Volly {
    public RequestQueue queue;
    public static Volly me;

    public Volly(Context context)
    {
        queue = Volley.newRequestQueue(context);
        me = this;
    }

    public static Volly GetInstance(Context context)
    {
        if(me == null)
        {
            me = new Volly(context);
            return me;
        }
        else
        {
            return me;
        }
    }

    public void AddToRequestQueue(StringRequest jor)
    {
        queue.add(jor);
    }

}
