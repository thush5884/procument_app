package com.proconstructor.com.proconstrctor.Gps;

import android.content.Context;
import android.test.AndroidTestCase;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by root on 11/24/17.
 */
public class GpsHelperTest extends AndroidTestCase {

    private Context instrumentationCtx;

    @Before
    public void setup() {
        instrumentationCtx = getContext();
    }

    @Test
    public void distance() throws Exception {

        double firstlong = 4.232324324;
        double secondlat = 12.223423423424;
        double thirdclong =1.232321324234;
        double forthclat = 0.234234234234;

        double distance = new GpsHelper(null,null).distance(firstlong,secondlat,thirdclong,forthclat,'K');
        assertNotEquals(0,distance);
    }

}