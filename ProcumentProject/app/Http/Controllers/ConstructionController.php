<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Construction;

class ConstructionController extends Controller
{
    //
    public function getAll(Request $request)
    {
        $contructions = Construction::where('company_id',$request->company_id)
        ->get();
        $response = ["data" => $contructions ];

        return $response;
    }
}
