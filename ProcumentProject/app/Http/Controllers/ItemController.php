<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    //
    public function Add(Request $request)
    {
        $item = new Item;
        $item->name = $request->name;
        $item->price = $request->price;
        $item->unit = $request->unit;
        $item->company_id = $request->company;
        $item->supplier_id = $request->supplier_id;
        $item->save();
        $response = [ "status" => 1 ];
        echo json_encode( $response);
    }

    public function GetAll(Request $request)
    {
        $items = Item::where('company_id',$request->company_id)
        ->where('supplier_id',$request->supplier_id)
        ->get();
        $response = ["data" => $items ];

        return $response;
    }

    public function Delete(Request $request)
    {
        $supplier = Item::find($request->id);
        $supplier->delete();
        $response = ["IsSuccess" => true];
        return json_encode($response);
    }

    public function Search(Request $request)
    {
        $items = Item::where('company_id',$request->company_id)
        ->where('supplier_id',$request->supplier_id)
        ->where('name','like', '%' . $request->stext . '%' )
        ->get();
        $response = ["data" => $items ];

        return $response; 
    }
}
