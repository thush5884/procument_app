<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    //
    public function AddNewOrder(Request $request)
    {
        $order = new Order;
        $order->construction_id = $request->construction_id;
        $order->supplier_id = $request->supplier_id;
        $order->company_id = $request->company_id;
        $order->items = $request->i_list;
        $order->order_status = 0;
        $order->save();

        $response = [ "status" => 1 ];
        echo json_encode( $response);


    }

    public function GetAllOrders(Request $request)
    {
        $orders = Order::where('company_id',$request->company_id)
        ->get();
        $response = ["data" => $orders];
        return json_encode($response);
    }

    public function Delete(Request $request)
    {
        $order = Order::find($request->id);
        $order->delete();
        $response = ["IsSuccess" => true];
        return json_encode($response);
    }

    public function Accept(Request $request)
    {
        $order = Order::find($request->id);
        $order->order_status = 1;
        $order->save();
        $response = ["IsSuccess" => true];
        return json_encode($response);
    }

    public function Decline(Request $request)
    {
        $order = Order::find($request->id);
        $order->order_status = 2;
        $order->save();
        $response = ["IsSuccess" => true];
        return json_encode($response);
    }
}
