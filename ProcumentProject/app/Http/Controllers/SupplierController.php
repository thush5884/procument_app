<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SupplierController extends Controller
{
    //
    public function AddSupplier(Request $request)
    {
         $supplier = new Supplier;
         $supplier->name = $request->name;
         $supplier->long = $request->lon;
         $supplier->lat = $request->lat;
         $supplier->company_id = $request->company_id;
         $supplier->email = $request->email;
         $supplier->phone = $request->contact;
         $supplier->save();

         $response = [ "status" => 1 ];
         echo json_encode( $response);
    }

    public function GetAll(Request $request)
    {
        $suppliers = Supplier::where('company_id',$request->company_id)
                               ->get();
        $response = ["data" => $suppliers];
        return json_encode($response);
    }

    public function Delete(Request $request)
    {
        $supplier = Supplier::find($request->id);
        $supplier->delete();
        $response = ["IsSuccess" => true];
        return json_encode($response);
    }
}
