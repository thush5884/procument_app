<?php

use Illuminate\Database\Seeder;

class ConstructionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('constructions')->insert([
            'name' => 'Site 1',
            'company_id' => 1
        ]);
        DB::table('constructions')->insert([
            'name' => 'Site 2',
            'company_id' => 1
        ]);
        DB::table('constructions')->insert([
            'name' => 'Site 3',
            'company_id' => 1
        ]);
    }
}
