<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'role_name' => 'Supervisor'
        ]);
        DB::table('roles')->insert([
            'role_name' => 'SiteManager'
        ]);
        DB::table('roles')->insert([
            'role_name' => 'Procument Department'
        ]);
        DB::table('roles')->insert([
            'role_name' => 'Finance Manager'
        ]);
    }
}
