<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Anton Thushara',
            'email' => 'thushara5884@hotmail.com',
            'password' => bcrypt('secret'),
            'company_id' => 1,
            'role_id' => 3
        ]);

        DB::table('users')->insert([
            'name' => 'Saman Perera',
            'email' => 'saman@hotmail.com',
            'password' => bcrypt('secret'),
            'company_id' => 1,
            'role_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => 'Nimal Perera',
            'email' => 'nimal@hotmail.com',
            'password' => bcrypt('secret'),
            'company_id' => 1,
            'role_id' => 1
        ]);
    }
}
