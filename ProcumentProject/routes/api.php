<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::middleware('auth:api')->group(function () {
    Route::get('/user', function (Request $request) {
        // Uses first & second Middleware
        return $request->user();
    });
    Route::get('/supplier','SupplierController@GetAll');
    Route::get('/item','ItemController@GetAll');
    Route::get('/item/search','ItemController@Search');
    Route::get('/order','OrderController@GetAllOrders');
    Route::get('/order/accept','OrderController@Accept');
    Route::get('/order/decline','OrderController@Decline');

    Route::middleware('procumentdepartment')->group(function () {
        Route::post('/supplier','SupplierController@AddSupplier');
        Route::delete('/supplier','SupplierController@Delete');
        Route::post('/item','ItemController@Add');
        Route::Delete('/item','ItemController@Delete');
        
        
        
    });

    Route::middleware('siteManager')->group(function () {
        Route::get('/construction','ConstructionController@GetAll');
        Route::post('/order','OrderController@AddNewOrder');
        Route::delete('/order','OrderController@Delete');
        
        

    });

});
